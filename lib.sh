waitForInput() {
  echo " "
  read -p "Press any key to continue... " -n1
  echo " "
  echo " "
}

printBanner() {
  echo " "
  echo "Talks: Java Forum Stuttgart 2019 Demo - Part $1"
  echo "**********************************************"
  echo " "
  echo "I will carry out these steps:"
}

printStep() {
  echo " "
  echo " "
  echo $1
  echo " "
}

commit2Git() {
  git add *
  echo "Committing to Git..."
  git commit -m "$1"
}


push2Git() {
  echo "Pushing to Gitlab..."
  git push
}