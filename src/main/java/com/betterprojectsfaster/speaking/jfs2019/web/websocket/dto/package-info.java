/**
 * Data Access Objects used by WebSocket services.
 */
package com.betterprojectsfaster.speaking.jfs2019.web.websocket.dto;
