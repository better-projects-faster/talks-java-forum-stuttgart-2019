/**
 * View Models used by Spring MVC REST controllers.
 */
package com.betterprojectsfaster.speaking.jfs2019.web.rest.vm;
