package com.betterprojectsfaster.speaking.jfs2019.domain.enumeration;

/**
 * The ProductCategory enumeration.
 */
public enum ProductCategory {
    Laptop, Desktop, Phone, Tablet, Accessory
}
