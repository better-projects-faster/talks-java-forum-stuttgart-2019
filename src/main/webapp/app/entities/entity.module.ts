import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'product',
        loadChildren: './product/product.module#BpfJfs2019ProductModule'
      },
      {
        path: 'address',
        loadChildren: './address/address.module#BpfJfs2019AddressModule'
      },
      {
        path: 'shopping-order',
        loadChildren: './shopping-order/shopping-order.module#BpfJfs2019ShoppingOrderModule'
      },
      {
        path: 'product-order',
        loadChildren: './product-order/product-order.module#BpfJfs2019ProductOrderModule'
      },
      {
        path: 'shipment',
        loadChildren: './shipment/shipment.module#BpfJfs2019ShipmentModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BpfJfs2019EntityModule {}
