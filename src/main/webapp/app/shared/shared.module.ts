import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BpfJfs2019SharedCommonModule, BpfLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [BpfJfs2019SharedCommonModule],
  declarations: [BpfLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [BpfLoginModalComponent],
  exports: [BpfJfs2019SharedCommonModule, BpfLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BpfJfs2019SharedModule {
  static forRoot() {
    return {
      ngModule: BpfJfs2019SharedModule
    };
  }
}
