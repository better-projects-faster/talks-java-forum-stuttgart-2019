import { NgModule } from '@angular/core';

import { BpfJfs2019SharedLibsModule, FindLanguageFromKeyPipe, BpfAlertComponent, BpfAlertErrorComponent } from './';

@NgModule({
  imports: [BpfJfs2019SharedLibsModule],
  declarations: [FindLanguageFromKeyPipe, BpfAlertComponent, BpfAlertErrorComponent],
  exports: [BpfJfs2019SharedLibsModule, FindLanguageFromKeyPipe, BpfAlertComponent, BpfAlertErrorComponent]
})
export class BpfJfs2019SharedCommonModule {}
