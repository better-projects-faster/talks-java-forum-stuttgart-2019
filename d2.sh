#!/bin/sh

source "$(dirname $0)/lib.sh"

printBanner "2"

echo "  1. Deploy Docker Image to Docker Hub"
echo "  2. Prepare Automated Browser Tests"
waitForInput

printStep "Step 1: Deploy to Docker Hub"
echo "Modifying Docker Compose File..."
rm src/main/docker/app.yml
cp etc/app.yml src/main/docker/
cp src/main/docker/app.yml ~/Desktop
commit2Git "Combined Docker Compose Files into one for Azure"
echo "Building application for release..."
./gradlew clean jibDockerBuild -Pprod
docker image tag bpf_jfs_2019:latest bpf_jfs_2019:1.0.0
echo "Pushing application Docker Image to Docker Hub..."
docker push joedata/bpf_jfs_2019:1.0.0
commit2Git "Built application Docker image"
push2Git


printStep "Step 2. Prepare Automated Browser Tests"
./gradlew clean bootRun
