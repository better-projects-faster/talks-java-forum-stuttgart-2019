#!/bin/sh

source "$(dirname $0)/lib.sh"

printBanner "1"

echo "  1. Generate Angular / Spring Boot application through JHipster by importing a JDL file"
echo "  2. Push generated application to public Gitlab project" 
echo "  3. Generate Gitlab Pipeline for Continuous Integration through JHipster and start it"
echo "  4. Run JHipster application locally"
waitForInput

printStep "Step 1: Generate Angular / Spring Boot application through JHipster by importing a JDL file"
jhipster import-jdl simple-shop-app.jdl


printStep "Step 2. Push generated application to public Gitlab project"
echo "Fixing wrong test data..."
cp etc/shipment.csv src/main/resources/config/liquibase/data
commit2Git "Status right after initial JHipster run"
echo "Pushing Git repository to Gitlab..."
git remote add origin git@gitlab.com:better-projects-faster/talks-java-forum-stuttgart-2019.git
git push -u origin --all
git push -u origin --tags


printStep "Step 3. Generate Gitlab Pipeline for Continuous Integration through JHipster and start it"
echo "Create Gitlab Pipeline file for Continuous Integration..."
./generate-gitlab-ci.sh
cat ./etc/gitlab-ci-fix.txt .gitlab-ci.yml > temp-gitlab-ci.yml
mv temp-gitlab-ci.yml .gitlab-ci.yml
git add .gitlab-ci.yml
echo "Committing to Git..."
git commit -m "Added Gitlab CI configuration" --no-verify
push2Git


printStep "Step 4. Run JHipster application locally"
./gradlew 

# manually started in another terminal, in parallel to last step
# ./gradlew npm_run_start
